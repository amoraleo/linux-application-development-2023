#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

#define BUF_SIZE 50

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Error: wrong arguments number (should be ./move infile outfile)\n");
        exit(1);
    }

    if (strcmp(argv[1], argv[2]) == 0) {
        exit(0);
    }

    char buf[BUF_SIZE];
    FILE *infile = fopen(argv[1], "r");
    if (infile == NULL) {
        printf("Error: couldn't open %s\n", argv[1]);
        exit(2);
    }
    FILE *outfile = fopen(argv[2], "w+");
    if (outfile == NULL) {
        printf("Error: couldn't open %s\n", argv[2]);
        if (fclose(infile) != 0) {
            printf("Error: couldn't close %s\n", argv[1]);
        }
        exit(3);
    }
    setvbuf(outfile, NULL, _IONBF, 0);

    while(fgets(buf, BUF_SIZE, infile)) {
        if (fputs(buf, outfile) == EOF) {
            printf("Error: couldn't write to %s\n", argv[2]);
            if (fclose(infile) != 0) {
                printf("Error: couldn't close %s\n", argv[1]);
            }
            if (fclose(outfile) != 0) {
                printf("Error: couldn't close %s\n", argv[2]);
            }
            if (remove(argv[2]) != 0) {
                printf("Error: couldn't remove %s\n", argv[2]);
            }
            exit(5);
        }
    }
    if (ferror(infile)) {
        printf("Error: something happened during %s reading\n", argv[1]);
        if (fclose(infile) != 0) {
            printf("Error: couldn't close %s\n", argv[1]);
        }
        if (fclose(outfile) != 0) {
            printf("Error: couldn't close %s\n", argv[2]);
        }
        if (remove(argv[2]) != 0) {
            printf("Error: couldn't remove %s\n", argv[2]);
        }
        exit(4);
    }

    if (fclose(infile) != 0) {
        printf("Error: couldn't close %s\n", argv[1]);
        if (fclose(outfile) != 0) {
            printf("Error: couldn't close %s\n", argv[2]);
        }
        if (remove(argv[2]) != 0) {
            printf("Error: couldn't remove %s\n", argv[2]);
        }
        exit(6);
    }
    if (fclose(outfile) != 0) {
        printf("Error: couldn't close %s\n", argv[2]);
        if (remove(argv[2]) != 0) {
            printf("Error: couldn't remove %s\n", argv[2]);
        }
        exit(7);
    }
    if (remove(argv[1]) != 0) {
        printf("Error: couldn't remove %s\n", argv[1]);
        exit(8);
    }
    exit(0);
}