#include <ncurses.h>
#include <stdlib.h>
#include <string.h>

// abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz

void print_new_line(WINDOW *win, char *str, int line_max_len) {
    wclrtobot(win);
    wprintw(win, " %s", str);
    if (strlen(str) == line_max_len && str[line_max_len-1]!='\n') {
        wprintw(win, " ");
    }
    box(win, 0, 0);
    wrefresh(win);
    return;
}

void skip_unnecessary_endline(FILE *f, char *last_line) {              // there is a problem with extra, unnecessary \n when line length in file is equal to window width
    char c=fgetc(f);
    if ((c!='\n' || last_line[strlen(last_line)-1]=='\n') && c!=EOF) {
        fseek(f, -1,SEEK_CUR);
    }
    return;
}

void process_eof(WINDOW *win, char *str) {      // guarantees correct output when the last line of file is not empty
    int x, y;
    getyx(win, y, x);
    if (x!=0) {
        wmove(win, y, 0);
        wclrtobot(win);
        wprintw(win, " %s\n", str);
        box(win, 0, 0);
        wrefresh(win);
    }
    return;
}

int main(int argc, char *argv[]) {
    if (argc!=2) {
        printf("There should be only one command line argument: filename\n");
        return 1;
    }
    else {
        initscr();
        noecho();
        cbreak();

        printw("File: %s", argv[1]);
        refresh();

        char c;
        int win_width=COLS-6;
        int win_height=LINES-6;
        char *buf=malloc((win_width)*sizeof(char)); 
        FILE *f=fopen(argv[1], "r");

        WINDOW *win=newwin(win_height, win_width, 3, 3);
        keypad(win, TRUE);
        scrollok (win, TRUE);
        box(win, 0, 0); 
        wmove(win, 1, 0);

        for (int i=0; i<win_height-2; i++) {                   // initial filling
            if (fgets(buf, win_width-1, f)!=NULL) {
                print_new_line(win, buf, win_width-2);
                skip_unnecessary_endline(f, buf);
            }
            else if (feof(f)) {
                process_eof(win, buf);
                break;
            }
        }

        while((c=wgetch(win)) != 27) {                     // file scrolling with space key
            if (c==' ') {
                if (fgets(buf, win_width-1, f)!=NULL) {
                    print_new_line(win, buf, win_width-2);
                    skip_unnecessary_endline(f, buf);
                }
                else if (feof(f)) {
                    process_eof(win, buf);
                }
            }
        }

        fclose(f);
        endwin();
        free(buf);
        return 0;
    }
}

// abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz