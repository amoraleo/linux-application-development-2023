#include <string.h>
#include <stdlib.h>
#include "rhash.h"
#include "config.h"

#ifdef NEED_READLINE
#include <readline/readline.h>
#endif


int main(int argc, char* argv[]) {
    unsigned int rhash_id;
    size_t len;
    int flags, res, nread = 0;
    unsigned char digest[64];
    char output[130];
    char *line, *hash_algo_name, *input;

    rhash_library_init();

    while (1) {
        #ifdef NEED_READLINE
        line = readline(NULL);
        if (line == NULL) {
            nread = -1;
        }
        #else
        nread = getline(&line, &len, stdin);
        #endif
        if (nread == -1) {
            break;
        }

        hash_algo_name = strtok(line, " ");
        if (hash_algo_name == NULL) {
            fprintf(stderr, "Error: wrong argument number (should be hash_algo_name filename|\"message ...)\n");
            continue;
        }
        input = strtok(NULL, " ");
        if (input == NULL || !strcmp(input, "\n")) {
            fprintf(stderr, "Error: wrong argument number (should be hash_algo_name filename|\"message ...)\n");
            continue;
        }

        if (!strcmp(hash_algo_name, "md5") || !strcmp(hash_algo_name, "MD5")) {
            rhash_id = RHASH_MD5;
        } else if (!strcmp(hash_algo_name, "sha1") || !strcmp(hash_algo_name, "SHA1")) {
            rhash_id = RHASH_SHA1;
        } else if (!strcmp(hash_algo_name, "tth") || !strcmp(hash_algo_name, "TTH")) {
            rhash_id = RHASH_TTH;
        } else {
            fprintf(stderr, "Error: unknown hash algorithm\n");
            continue;
        }

        if ('a' <= hash_algo_name[0] && hash_algo_name[0] <= 'z') {
            flags = (RHPR_BASE64);
        } else if ('A' <= hash_algo_name[0] && hash_algo_name[0] <= 'Z') {
            flags = (RHPR_HEX);
        }

        char *to_hash;

        if (input[0] == '"') {
            to_hash = input+sizeof(char);
            if (to_hash[strlen(to_hash)-1] == '\n') to_hash[strlen(to_hash)-1] = '\0';
            res = rhash_msg(rhash_id, to_hash, strlen(to_hash), digest);
            if (res < 0) {
                fprintf(stderr, "Error: failed to compute a hash of the given message %s\n", to_hash);
                continue;
            }
        } else {
            to_hash = input;
            if (to_hash[strlen(to_hash)-1] == '\n') to_hash[strlen(to_hash)-1] = '\0';
            res = rhash_file(rhash_id, to_hash, digest);
            if (res < 0) {
                fprintf(stderr, "Error: failed to compute a hash of the given file %s\n", to_hash);
                continue;
            }
        }

        rhash_print_bytes(output, digest, rhash_get_digest_size(rhash_id), flags);
        printf("%s(\"%s\") = %s\n", rhash_get_name(rhash_id), to_hash, output);
    }
    return 0;
}