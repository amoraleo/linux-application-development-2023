#! /usr/bin/bash

if [ -z $1 ]
then
    DELAY=0.1
else
    DELAY=$1
fi

CURR_LINE_NUM=0

while IFS= read -r LINE
do
    LINELEN=${#LINE}
    for ((CURR_COL_NUM=0; CURR_COL_NUM<${LINELEN}; CURR_COL_NUM++))
    do
        if [ "${LINE:$CURR_COL_NUM:1}" != " " ]
        then
            CHARS_WITH_COORDS+="${LINE:CURR_COL_NUM:1} $CURR_LINE_NUM $CURR_COL_NUM\n"
        fi
    done
    CURR_LINE_NUM=$((CURR_LINE_NUM+1))
done

SHUFFLED_CHARS_WITH_COORDS=$(echo -en $CHARS_WITH_COORDS | shuf)
IFS=$'\n'

tput clear

for LINE in $SHUFFLED_CHARS_WITH_COORDS
do
    readarray -d " " -t ARRAY <<< $LINE
    tput cup ${ARRAY[1]} ${ARRAY[2]}
    echo -n ${ARRAY[0]}
    sleep $DELAY
done

tput cup $CURR_LINE_NUM 0