#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAZE_SIZE 6

char maze[2 * MAZE_SIZE + 1][2 * MAZE_SIZE + 1];

void build_maze(int room_x, int room_y) {
    const int shift_x[4] = {2, 0, -2, 0};
    const int shift_y[4] = {0, 2, 0, -2};
    int new_room_x, new_room_y, curr_dir_num;

    maze[room_y][room_x] = '.';
    int dirs[4] = {0, 1, 2, 3};

    for (int i = 3; i > 0; i--) {
        int j = rand() % (i + 1);
        int temp_dir = dirs[i];
        dirs[i] = dirs[j];
        dirs[j] = temp_dir;
    }

    for (int i = 0; i < 4; i++) {
        curr_dir_num = dirs[i];
        new_room_x = room_x + shift_x[curr_dir_num];
        new_room_y = room_y + shift_y[curr_dir_num];
        if (new_room_x >= 0 && new_room_x < 2 * MAZE_SIZE + 1 && new_room_y >= 0 && new_room_y < 2 * MAZE_SIZE + 1 && maze[new_room_y][new_room_x] == '#') {
            maze[new_room_y - shift_y[curr_dir_num] / 2][new_room_x - shift_x[curr_dir_num] / 2] = '.';
            build_maze(new_room_x, new_room_y);
        }
    }
}

int main(int argc, char* argv[]) {
    srand(time(NULL));

    for (int y = 0; y < 2 * MAZE_SIZE + 1; y++) {
        for (int x = 0; x < 2 * MAZE_SIZE + 1; x++) {
            maze[y][x] = '#';
        }
    }

    build_maze(1, 1);

    for (int y = 0; y < 2 * MAZE_SIZE + 1; y++) {
        for (int x = 0; x < 2 * MAZE_SIZE + 1; x++) {
            printf("%c", maze[y][x]);
        }
        printf("\n");
    }

    return 0;
}