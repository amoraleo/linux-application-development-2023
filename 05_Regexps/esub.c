#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <string.h>

#define MAX_BAGS 10
#define DEFAULT_STR_SIZE 20
#define ERROR_BUF_SIZE 1000

int main(int argc, char *argv[]) {
    if (argc != 4) {
        printf("Error: Wrong arguments number (should be ./esub regexp substitution string)\n");
        return 1;
    }

    char *regexp = argv[1];
    char *substitution=argv[2];
    char *input = argv[3];

    regex_t regex;
    regmatch_t bags[MAX_BAGS];
    int errcode;

    if ((errcode = regcomp(&regex, regexp, REG_EXTENDED))) {
        char errbuf[ERROR_BUF_SIZE];
        regerror(errcode, &regex, errbuf, ERROR_BUF_SIZE);
        printf("Error: %s\n", errbuf);
        regfree(&regex);
        return 2;
    }
    if (regexec(&regex, input, MAX_BAGS, bags, 0) == REG_NOMATCH) {
        printf("%s\n", input);
        regfree(&regex);
        return 0;
    }

    char *substitution_without_bags = malloc(sizeof(char)*DEFAULT_STR_SIZE);
    int i, j, k;
    for (i=0, j=0; i<strlen(substitution)-1; i++) {
        if (j % DEFAULT_STR_SIZE == 0 && j != 0) {
            substitution_without_bags = realloc(substitution_without_bags, sizeof(char)*(j+DEFAULT_STR_SIZE));
        }
        if (substitution[i] == '\\') {
            if (substitution[i+1] >= '0' && substitution[i+1] <= '9') {
                int bag_start = bags[substitution[i+1]-'0'].rm_so, bag_end = bags[substitution[i+1]-'0'].rm_eo;
                if (bag_start == -1) {
                    printf("Error: Link to a non-existent bag %c%c in substitution string\n", substitution[i], substitution[i+1]);
                    free(substitution_without_bags);
                    regfree(&regex);
                    return 3;
                }
                char *bag = malloc(sizeof(char)*(bag_end-bag_start+1));
                for (k=bag_start; k<bag_end; k++) {
                    bag[k-bag_start] = input[k];
                }
                bag[bag_end-bag_start] = '\0';
                for (k=0; k<strlen(bag); k++, j++) {
                    if (j % DEFAULT_STR_SIZE == 0 && j != 0) {
                        substitution_without_bags = realloc(substitution_without_bags, sizeof(char)*(j+DEFAULT_STR_SIZE));
                    }
                    substitution_without_bags[j] = bag[k];
                }
                i += 1;
                free(bag);
            }
            else if (substitution[i+1] == '\\') {
                substitution_without_bags[j] = substitution[i];
                i += 1;
                j += 1;
            }
            else {
                substitution_without_bags[j] = substitution[i];
                j += 1;
            }
        }
        else {
            substitution_without_bags[j] = substitution[i];
            j += 1;
        }
    }
    if (i == strlen(substitution)-1) {
        substitution_without_bags[j] = substitution[i];
        substitution_without_bags[j+1] = '\0';
    }
    else {
        substitution_without_bags[j] = '\0';
    }

    int regex_start = bags[0].rm_so, regex_end = bags[0].rm_eo;
    char *res = malloc(sizeof(char)*(regex_start+strlen(substitution_without_bags)+strlen(input)-regex_end+1));

    for (i=0; i<regex_start; i++) {
        res[i] = input[i];
    }
    for (i=0; i<strlen(substitution_without_bags); i++) {
        res[regex_start+i] = substitution_without_bags[i];
    }
    for (i=0; i<strlen(input)-regex_end; i++) {
        res[regex_start+strlen(substitution_without_bags)+i] = input[regex_end+i];
    }
    res[regex_start+strlen(substitution_without_bags)+strlen(input)-regex_end] = '\0';
    printf("%s\n", res);

    free(res);
    free(substitution_without_bags);
    regfree(&regex);
    return 0;
}