#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int M=0;
    int i, N;
    int S=1;
    if (argc==1) {
        printf("There should be at least one command line argument:\n\
        1) ./range N generates sequence 0,1,...,N-1\n\
        2) ./range M N generates sequence M,M+1,...,N-1\n\
        3) ./range M N S generates sequence M,M+S,...,M+S*((N-M)//S) (the same with more than 3 arguments)\n");
        return 1;
    } else if (argc==2)
    {
        N=atoi(argv[1]);
    } else if (argc==3)
    {
        M=atoi(argv[1]);
        N=atoi(argv[2]);
    } else if (argc>=4)
    {
        M=atoi(argv[1]);
        N=atoi(argv[2]);
        S=atoi(argv[3]);
    }
    for (i=M; i<N; i=i+S) {
        printf("%d\n", i);
    }
    return 0;
}