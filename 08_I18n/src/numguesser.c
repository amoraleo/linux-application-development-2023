#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <locale.h>
#include <libintl.h>

#define MIN_NUM 1
#define MAX_NUM 100

#define _(STRING) gettext(STRING)

int main(int argc, char* argv[]) {
    int left, right, middle;
    char answer[4];

    setlocale (LC_ALL, "");
	bindtextdomain ("numguesser", "po");
	textdomain ("numguesser");

    printf(_("Pick a number from %d to %d\n"), MIN_NUM, MAX_NUM);
    sleep(1);

    left = MIN_NUM - 1;
    right = MAX_NUM;

    while(right != left + 1) {
        middle = (right + left) / 2;

        printf(_("Is your number greater than %d? [y/n]: "), middle);
        scanf("%s", answer);

        if (!strcmp(answer, _("y"))) {
            left = middle;
        } else if (!strcmp(answer, _("n"))) {
            right = middle;
        } else {
            printf(_("Incorrect answer: should be only y or n\n"));
            exit(1);
        }
    }
    
    printf(_("You picked number %d!\n"), right);
    return 0;
}